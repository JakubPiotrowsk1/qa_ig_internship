package exercises.ex2;

class NotATriangleException extends Exception {
    NotATriangleException(String s) {
        super(s);
    }
}
