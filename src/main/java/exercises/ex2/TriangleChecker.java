package exercises.ex2;


/**
 * The goal of this exercise is to implement a method below which checks triangle type out of given sides: int a, int b, int c
 * or throws NotATriangleException.
 * You should also implement unit tests verifying this method
 */
public class TriangleChecker {
    public static Triangle checkTriangleType(int a, int b, int c) throws NotATriangleException {
        if(checkValidTriangle(a,b,c)){
            if(a==b && b==c){
                return Triangle.EQUILATERAL;
            } else if(a!=b && b!=c && c!=a){
                return Triangle.OTHER;
            } else {
                return Triangle.ISOSCELES;
            }
        } else{
            throw new NotATriangleException("Invalid triangle!");
        }
    }
    public static boolean checkValidTriangle(int a, int b, int c) throws NotATriangleException {
        try{
            if(a<=0 || b<=0 || c<=0){
                throw new NotATriangleException("Arguments must be greater than 0");
            } else return a + b > c && a + c > b && b + c > a;
        } catch (NotATriangleException e) {
            throw new NotATriangleException("Arguments must be greater than 0");
        }
    }
}
