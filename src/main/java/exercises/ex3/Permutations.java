package exercises.ex3;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
/**
   As an input to this program, you provide word
   As a result it is expected that all permutations of letters in this word will be printed out as well as number of permutations
   Optional : verify if each permutation of a word is a valid word in https://sjp.pl/ dictionary

   Use Permutator class
   Add unit tests to validate if the program works properly
 */
public class Permutations {
   private Permutator permutator;

   public Permutations(Permutator permutator) {
      this.permutator = permutator;
   }

   public void printPermutations(){
      permutationsToList()
              .forEach(System.out::println);

      System.out.print("Number of permutations: " + permutationsToList().size());
   }
   public List<String> permutationsToList(){
      if(permutator.getInput() == null){
         return null;
      } else{
         return permutations(permutator.getInput())
                 .collect(Collectors.toList());
      }
   }
   private Stream<String> permutations(String word){
      if(word == null){
         return null;
      }else if(word.length() == 0){
         return Stream.of("");
      } else{
         return IntStream.range(0, word.length()).boxed()
                 .flatMap(i -> permutations(word.substring(0, i) + word.substring(i + 1)).map(t -> word.charAt(i) + t));
      }
   }
}
