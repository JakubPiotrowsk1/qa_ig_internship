package exercises.ex4;

/**
 Create application that will check if a given String is a palindrome or not
 As a result we should receive boolean value if the input word is a palindrome or not

 Add Junit tests that will verify if the application is written in a proper way

 Mandatory: Palindrome class must implement the PalindromeCheckerInterface
 */
public class Palindrome implements PalindromeCheckerInterface {
    @Override
    public boolean isPalindrome(String theWord) {
        if (theWord == null || theWord.equals("")) {
            return false;
        } else {
            String wordWithReplacedChars = theWord.replaceAll("[\\W]", "");
            StringBuilder stringBuilder = new StringBuilder(wordWithReplacedChars);
            if (wordWithReplacedChars.equalsIgnoreCase(stringBuilder.reverse().toString())) {
                return true;
            }
        }
        return false;
    }
    @Override
    public void printResult() {
    }
}