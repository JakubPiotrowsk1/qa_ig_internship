package exercises.ex4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;



public class PalindromeTest {
    private Palindrome palindrome = new Palindrome();

    @Test
    public void testRecognizeNull(){
        assertEquals(false, palindrome.isPalindrome(null));
    }

    @Test
    public void testNotPalindrome(){
        assertFalse(palindrome.isPalindrome("Ala ma kota"));
        assertFalse(palindrome.isPalindrome("Lorem ipsum dolor sit amet."));
        assertFalse(palindrome.isPalindrome("Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit"));

    }

    @Test
    public void testPhraseIsPalindrome(){
        assertTrue(palindrome.isPalindrome("A krowo, do worka!"));
        assertTrue(palindrome.isPalindrome("Ale uda szałowe me! – Woła z sadu Ela."));
        assertTrue(palindrome.isPalindrome("I lali masoni wydrom w mordy wino, sami lali."));
    }
    @Test
    public void testRecognizeEmptyString(){
        assertFalse(palindrome.isPalindrome(""));
    }
}
