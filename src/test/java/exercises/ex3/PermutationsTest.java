package exercises.ex3;

import org.junit.Assert;
import org.junit.Test;
import java.util.*;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
public class PermutationsTest {

    @Test
    public void testIfAllPermutationsAreValid(){
        Permutations p = new Permutations(new Permutator("abc"));
        List<String> expectedResult = Arrays.asList("acb", "abc", "bac", "bca", "cab", "cba");
        List<String> listOfPermutations = p.permutationsToList();
        assertThat(listOfPermutations, containsInAnyOrder(expectedResult.toArray()));
    }
    @Test
    public void testIfNumberOfPermutationsAreValid(){
        Permutations p = new Permutations(new Permutator("abc"));
        assertEquals(p.permutationsToList().size(), 6);
    }

    @Test
    public void testIfInputIsNull(){
        Permutations p = new Permutations(new Permutator(null));
        Assert.assertNull(p.permutationsToList());
    }
}