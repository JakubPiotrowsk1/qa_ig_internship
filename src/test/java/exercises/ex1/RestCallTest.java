package exercises.ex1;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.hasItem;

/**
 * The purpose of this test is to check that unauthorized user is not able to get pending agreements.
 *
 * 1. Send GET request without headers to https://api.ig.com/usermanagement/pendingagreements. Please use REST-assured (http://rest-assured.io) to do it.
 * 2. Check if:
 *    - response code is 401
 *    - global error message is: "error.security.client-token-missing"
 * 
 * You can additinally use these libraries: Junit, TestNg, AssertJ  
 */
public class RestCallTest {

    @Test
    public void testIfResponseCodeIs401(){
        RestAssured.baseURI = "https://api.ig.com";
        given().get("usermanagement/pendingagreements")
                .then().assertThat().statusCode(401);

    }

    @Test
    public void testGlobalErrorMessage()
    {
        RestAssured.baseURI = "https://api.ig.com";
        given().get("usermanagement/pendingagreements")
                .then().assertThat().statusCode(401)
                .body("globalErrors",hasItem("error.security.client-token-missing"));
    }
}
