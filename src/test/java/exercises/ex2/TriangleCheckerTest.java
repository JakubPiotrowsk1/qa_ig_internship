package exercises.ex2;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static exercises.ex2.Triangle.EQUILATERAL;
import static exercises.ex2.Triangle.ISOSCELES;
import static exercises.ex2.Triangle.OTHER;
import static org.junit.Assert.assertEquals;


/**
 * unit tests for TriangleChecker should be implemented here
 */
public class TriangleCheckerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testTriangleContainsNegativeNumbers() throws NotATriangleException {
        thrown.expect(NotATriangleException.class);
        thrown.expectMessage("Arguments must be greater than 0");
        TriangleChecker.checkTriangleType(-1,2,3);
    }

    @Test
    public void testTriangleIsIsosceles() throws NotATriangleException {
        assertEquals(ISOSCELES, TriangleChecker.checkTriangleType(2,2,3));
        assertEquals(ISOSCELES, TriangleChecker.checkTriangleType(3,4,4));
        assertEquals(ISOSCELES, TriangleChecker.checkTriangleType(3,2,3));
    }

    @Test
    public void testTriangleIsEquilateral() throws NotATriangleException {
        assertEquals(EQUILATERAL, TriangleChecker.checkTriangleType(3,3,3));
        assertEquals(EQUILATERAL, TriangleChecker.checkTriangleType(5,5,5));
    }

    @Test
    public void testTriangleContainsZero() throws NotATriangleException {
        thrown.expect(NotATriangleException.class);
        thrown.expectMessage("Arguments must be greater than 0");
        TriangleChecker.checkTriangleType(0,1,3);
    }

    @Test
    public void testInvalidTriangle() throws NotATriangleException {
        thrown.expect(NotATriangleException.class);
        thrown.expectMessage("Invalid triangle!");
        TriangleChecker.checkTriangleType(1,2,3);
    }

    @Test
    public void testOtherTriangle() throws NotATriangleException {
        assertEquals(OTHER, TriangleChecker.checkTriangleType(6,12,16));
        assertEquals(OTHER, TriangleChecker.checkTriangleType(7,6,11));
    }





}